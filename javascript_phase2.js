<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Animal Trading Cards</title>
	<link rel="stylesheet" type="text/css" href="styles.css">
</head>
<body>
	<div class="animal-info" id="container">
		<!-- your favorite animal's name goes here -->
		<h1>Hariyama</h1>
			<div class="image">
		<img src="https://www.pokewiki.de/images/3/33/Sugimori_297.png" width="210px" alt="Hariyama">
	</div>
		<div id="card" class="animal-info">
			<!-- your favorite animal's interesting fact goes here -->
			<p id="interesting-fact">It's habitat consists of mountainous areas.</p>
			<ul id="facts">
				<!-- your favorite animal's list items go here -->
				<li value="none"><span>Evoles from</span>: Makuhita</li>
				<li><span>Generation</span>: 3</li>
				<li><span>Type</span>: Fighting</li>
				<li><span>Evolves from Makuhita at level</span>: 24</li>
			</ul>
			<!-- your favorite animal's description goes here -->
			<p id="summary">Snaps telephone poles in half, and trains can even be stopped by its powerful arms.</p>
		</div>
	</div>
</body>
</html>
